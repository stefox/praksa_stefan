<?php


namespace Drupal\facebook;

use Drupal\menu_link_content\Entity\MenuLinkContent;



/**
 * extend Drupal's Twig_Extension class
 */
class customTwigExtension extends \Twig_Extension {
  /**
   * {@inheritdoc}
   * Let Drupal know the name of your extension
   * must be unique name, string
   */


  public function getName() {
    return 'facebook.ui_utils_extension';
  }
  /**
   * {@inheritdoc}
   * Return your custom twig function to Drupal
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('get_url_param', [$this, 'get_url_param']),
      new \Twig_SimpleFunction('getSettings', array($this, 'getSettings'), array('is_safe' => array('html'))),
    ];
  }
  /**
   * {@inheritdoc}
   * Return your custom twig filter to Drupal
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('replace_tokens', [$this, 'replace_tokens']),
      new \Twig_SimpleFilter('get_settings', [$this, 'getSettings'], ['is_safe' => ['html']]),
    ];
  }
  /**
   * Returns $_GET query parameter
   *
   * @param string $name
   *   name of the query parameter
   *
   * @return string
   *   value of the query parameter name
   */
  public function get_url_param($name) {
    return \Drupal::request()->query->get($name);
  }
  /**
   * Replaces available values to entered tokens
   * Also accept HTML text
   *
   * @param string $text
   *   replaceable tokens with/without entered HTML text
   *
   * @return string
   *   replaced token values with/without entered HTML text
   */
  public function replace_tokens($text) {
    return \Drupal::token()->replace($text);
  }

  public function getSettings($test) {
    //Getting URL from configuration form
    $config = \Drupal::configFactory()->get('facebook.settings');
    $url = $config->get('enteredUrl');


    $addNew = true; //boolean used to see if URL already exists in menu item
    $my_menu = \Drupal::entityTypeManager()->getStorage('menu_link_content')
      ->loadByProperties(['menu_name' => 'main']);
    foreach ($my_menu as $menu_item) {
      $currentMenuLink = $menu_item->link->uri;
      if($currentMenuLink == $url) {
        $addNew = false;
        break;
      }
    }

    if($addNew) {
      $top_level = "<Main navigation>";
      $currentId = $this->getCurrentId($my_menu);

      $updateMenuItem = MenuLinkContent::load($currentId);
      if($updateMenuItem != null) { //if menu already exists update it otherwise create new
        $updateMenuItem->set('link', ['uri' => $url]);
        $updateMenuItem->save();
      } else {
        $createNewMenuItem = MenuLinkContent::create([
          'title' => 'Facebook',
          'link' => ['uri' => $url],
          'menu_name' => 'main',
          'parent' => $top_level,
          'expanded' => TRUE,
          'weight' => 0,
        ]);
        $createNewMenuItem->save();
      }
    }
    return strtoupper($test);
  }

  /**
   * @param $my_menu
   *
   * @return string
   * Getting ID of current facebook menu item, if it exists
   */
  public function getCurrentId($my_menu) {
    $currentId = '20';
    foreach ($my_menu as $menu_item) {
      $title = $menu_item->getTitle();
      if($title == 'Facebook') {
        $currentId = $menu_item->id();
        break;
      }
    }
    return $currentId;
  }

}
