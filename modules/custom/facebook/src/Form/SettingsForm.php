<?php

namespace Drupal\facebook\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure facebook settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facebook_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['facebook.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('facebook.settings');

    $form['enteredUrl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Enter facebook link you want shown in menu'),
      '#description' => $this->t('Please provide custom link you want displayed in menu.'),
      '#default_value' => $config->get('enteredUrl'),
    );

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('facebook.settings')
      ->set('enteredUrl', $form_state->getValue('enteredUrl'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
