<?php

/**
 * Implements controller that displays title and simple twig template
 */

namespace Drupal\test_twig_template\Controller;

use Drupal\Core\Controller\ControllerBase;

class TestTwigTemplateController extends ControllerBase {
  public function content() {
    return [
      '#title' => 'Test Twig title',
      '#markup' => 'Here is some content about dogs and their everyday activity',
      '#theme' => 'test_twig_template',
      '#test_var' => $this->t('Value used for testing 123'),
    ];
  }
}
