<?php

/**
 * Implements controller that displays simple custom page
 */

namespace Drupal\cool_dogs\Controller;

use Drupal\Core\Controller\ControllerBase;

class CoolDogsController extends ControllerBase {
  public function cool() {
    return array(
      '#title' => 'Cool Dogs Module!',
      '#markup' => 'Here is some content about dogs and their everyday activity',
      '#theme' => 'cool_dogs',
      '#test_var' => $this->t('Test Value sent from PHP to theme'),
    );

  }

}


