<?php

namespace Drupal\books\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class CustomControlForm extends ConfigFormBase {

  protected function getEditableConfigNames()
  {
    return ['books.control'];
  }

  public function getFormId()
  {
    return 'control_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('books.control');

    $form['enteredUrl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Link to facebook page'),
      '#description' => $this->t('Please provide link to fb page you want to see in menu.'),
      '#default_value' => $config->get('enteredUrl'),
    );

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('books.control')
      ->set('enteredUrl', $form_state->getValue('enteredUrl'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
