<?php

namespace Drupal\books\Controller;


use Drupal\books\Services\BooksService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\Exception\RequestException;
use SimpleXMLElement;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;
use Drupal\node\Entity\Node;


/**
 * Returns responses for Books routes.
 */
class BooksController extends ControllerBase {
  protected $entityTypeManager;
  protected $booksService;

  public function __construct(EntityTypeManagerInterface $entityTypeManager,  $booksService){
    $this->entityTypeManager = $entityTypeManager;
    $this->booksService = $booksService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('books.xml')
    );
  }
  /**
   * Builds the response.
   */
  public function build() {
    $url = 'http://www.chilkatsoft.com/xml-samples/bookstore.xml';

    $myService = $this->booksService;
    $xmlData = $myService->getXmlFromURL($url);
    $bookstore = $myService->convertXmlToArray($xmlData);
    $this->createBooks($bookstore);

    $nodes = $this->getNodes();
    $books = $this->getBooks($nodes);

    return array(
      '#title' => t('Books from XML'),
      '#books' => $books,
      '#theme' => 'books',
    );
  }

  /**
   * @param $nodes
   *
   * @return array
   * Preparing render array
   */
  public function getBooks($nodes) {
    $books = array();
    foreach ($nodes as $node) {
      $books[] = array(
        'title' => $node->getTitle(),
        'isbn' => $node->get('field_isbn')->value,
        'price' => $node->get('field_price')->value,
        'comments' => $this->getComments($node),
      );
    }
    return $books;
  }

  /**
   * @param $node
   *
   * @return array
   * Getting comments from each node
   */
  public function getComments($node) {
    $comments = array();
    $all = $node->get('field_comments');
    foreach ($all as $one) {
      $comments[] = $one->value;
    }
    return $comments;
  }

  public function createBooks($bookstore) {
    foreach ($bookstore as $book) {
      foreach ($book as $item) {
        $arrayComments = !empty($item['comments']['userComment']) ?  $item['comments']['userComment'] : null;
        $comments = array();
        if(is_array($arrayComments)) {
          foreach ($arrayComments as $comment) {
            $one = trim($comment, " \n\t. ");
            $comments[] = $one;
          }
        } else {
          $one = trim($arrayComments, " \n\t. ");
          $comments[] = $one;
        }

        $createNew = true;
        $allNodes = $this->getNodes();
        foreach ($allNodes as $oneNode) {
          if($item["@attributes"]['ISBN'] == ($oneNode->get('field_isbn')->value) ) {
            $createNew = FALSE;
            break;
          }
        }
        if($createNew) {
          $node = array(
            'type' => 'book',
            'uid' => '1',
            'title' => $item['title'],
            'field_price' => $item['price'],
            'field_isbn' => $item["@attributes"]['ISBN'],
            'field_comments' => $comments,
          );

          $this->EntityTypeManager()->getStorage('node')->create($node)->save();
        }
      }
    }
  }

  public function getNodes() {
    $node_storage = $this->EntityTypeManager()->getStorage('node');

    $query = $this->entityTypeManager()->getStorage('node')->getQuery()
      ->condition('type', 'book')
      ->execute();

    $nodes = $node_storage->loadMultiple($query);

    return $nodes;
  }

}
