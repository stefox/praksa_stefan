<?php

namespace Drupal\books\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class BooksService {
  protected $entityTypeManager;
  protected $httpClient;

  public function __construct(Client $httpClient){
    $this->httpClient = $httpClient;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * @param $url
   *
   * @return bool|mixed
   * Getting XML data from external link
   */
  public function getXmlFromURL($url) {
    try {
      $response = $this->httpClient->get($url, array('headers' => array('Accept' => 'text/xml')));
      $data = (string) $response->getBody();
      if (empty($data)) {
        return FALSE;
      }
      return $data;
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

  /**
   * @param $data
   *
   * @return mixed
   * Converting input XML data into an array
   */
  public function convertXmlToArray($data) {
    $xml = simplexml_load_string($data);
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);

    return $array;
  }
}
