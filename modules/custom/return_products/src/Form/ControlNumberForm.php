<?php

namespace Drupal\return_products\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ControlNumberForm
 * @package Drupal\return_products\Form
 * Class to define number of products per page
 */
class ControlNumberForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames()
  {
     return ['return_products.custom_control'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
     return 'control_number_form';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   * Returns form definition (array of form element definitions)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('return_products.custom_control');

    $form['enteredNumber'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Control number of products on page'),
      '#description' => $this->t('Please provide custom number of products you want to see on page.'),
      '#default_value' => $config->get('enteredNumber'),
      );

    return parent::buildForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Handler that is is called when form is submitted (if we have validation it has to pass without errors)
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('return_products.custom_control')
      ->set('enteredNumber', $form_state->getValue('enteredNumber'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Checking if user has entered a number
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $enteredNumber = $form_state->getValue('enteredNumber');
    if (filter_var($enteredNumber, FILTER_VALIDATE_INT) === false) {
      $form_state->setErrorByName('enteredNumber', $this->t('This is not a number'));
    }
  }
}
