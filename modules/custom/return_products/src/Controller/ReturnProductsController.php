<?php

/**
 * Implements controller that returns all products with their name, description and image
 */

namespace Drupal\return_products\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\NodeLabel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactory;


class ReturnProductsController extends ControllerBase {

  protected $entityTypeManager;
  protected $requestStack;
  protected $configFactory;
  protected $cacheBackend;

  public function __construct(EntityTypeManagerInterface $entityTypeManager,RequestStack $requestStack, ConfigFactory $configFactory, CacheBackendInterface $cache_backend){
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
    $this->configFactory = $configFactory;
    $this->cacheBackend = $cache_backend;
  }

  //EntityQuery is being deprecated and all functions that we need are now in EntityTypeManager
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('return_products.my_cache')
    );
  }

  /**
   * Showing list of all children products shown on /children
   */
  public function children() {
    $nodes = $this->filterData(null, ''); //calling it like this because I just want to create it but didn't want to create a new method just for that

    $products = $this->getProductsChildren($nodes);

    $terms = $this->getTaxonomyTerms();

    return array(
      '#title' => t('Product without parents'),
      '#products' => $products,
      '#pages' => [
        'numberOfPages' => 1,
      ],
      '#terms' => $terms,
      '#theme' => 'return_products',
    );
  }

  /**
   * Showing just products without parent shown on page /products-parent
   */
  public function parent() {
    $nodes = $this->filterData(null, '');

    $products = $this->getProductsNoParents($nodes);

    $terms = $this->getTaxonomyTerms();

    //using EntityTypeManager since it caches results???
//    $menuItemId = $this->getMenuItemId();
//    $entity = $this->entityTypeManager->getStorage('menu_link_content')->load($menuItemId);

    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    $cid = 'return_products:' . $user->id();
    $data_cached = $this->cacheBackend->get($cid);

//  Check cache, if it is not empty get products array from it, if it is empty set cache to current product array
    if (is_array($data_cached->data)) {
      $products = $data_cached->data;
    }
    else {
      $setCache = $this->cacheBackend->set($cid, $products);
    }

    return array(
      '#title' => t('Product without parents'),
      '#products' => $products,
      '#pages' => [
        'numberOfPages' => 1,
      ],
      '#terms' => $terms,
      '#theme' => 'return_products',
      '#cache' => [
        'max-age' => 300, // if we want to set cache to expire after 5 minutes just for this page
      ],
    );
  }

  public function content() {
      //Getting submitted input from twig template
      $request = $this->requestStack->getCurrentRequest();

      $twigValue = $request->query->get('twigValue');
      $currentlySelected = !empty($request->query->get('selectTag')) ? $request->query->get('selectTag') : '';

      $nodes = $this->filterData($twigValue, $currentlySelected);

      $products = $this->getProducts($nodes);

//      kint($products);

      $productsOnPage = intval($this->getValueFromForm());
      $numberOfPages = $this->getNumberOfPages($products, $productsOnPage);
      $currentPage = !empty($request->query->get('pages')) ? $request->query->get('pages') : 1;

      $products = $this->getProductsForPage($currentPage, $products, $productsOnPage);

      //All tags we have in our vocabulary
      $terms = $this->getTaxonomyTerms();

      return array(
        '#title' => t('Product listing page'),
        '#products' => $products,
        '#pages' => [
           'numberOfPages' => $numberOfPages,
        ],
        '#terms' => $terms,
        '#theme' => 'return_products',
      );
  }

  /**
   * @param $nodes
   * @return array
   * Preparing all products into one array that is ready to be used in render array
   */
  public function getProducts($nodes) {
    $products = [];
    foreach ($nodes as $product) {
      $file = $product->field_image->entity;

      $products[] = array(
        'title' => $product->title->value,
        'image' => file_url_transform_relative(file_create_url($file->getFileUri())),
        'description' => $product->body->value,
        'tags' => $this->getTaxonomy($product),
      );
    }

    return $products;
  }

  /**
   * @param $twigValue variable used to filter by TITLE
   * @param $currentlySelected variable used to filter by TAG
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * Depending what filters we have entered we filer data
   * If we have just one filter (eg. title) it will be filtered by title,
   * same with tags. If we have entered both filters then both will be applied
   * Or if we have no filters applied then we will just get all nodes from entity type
   */
  public function filterData($twigValue, $currentlySelected){
    if($twigValue == null and $currentlySelected == '') {
      //Getting typeManager for type node that we will later use to get all nodes into one node
      $node_storage = $this->EntityTypeManager()->getStorage('node');

      //Going trough entities of type 'products_for_praksa' to get their IDs
      $query = $this->entityTypeManager()->getStorage('node')->getQuery()
        ->condition('type', 'products_for_praksa')
        ->execute();

      //Getting all nodes into one
      $nodes = $node_storage->loadMultiple($query);

      return $nodes;
    }

    if($twigValue != null and $currentlySelected == '') {
      //Getting typeManager for type node that we will later use to get all nodes into one node
      $node_storage = $this->EntityTypeManager()->getStorage('node');

      //Going trough entities of type 'products_for_praksa' to get their IDs
      $query = $this->entityTypeManager()->getStorage('node')->getQuery()
        ->condition('type', 'products_for_praksa')
        ->condition('title', $twigValue)
        ->execute();
      //Getting all nodes into one
      $nodes = $node_storage->loadMultiple($query);

      return $nodes;
    }
    if($twigValue == null and $currentlySelected != '') {
      //Getting typeManager for type node that we will later use to get all nodes into one node
      $node_storage = $this->EntityTypeManager()->getStorage('node');

      //Going trough entities of type 'products_for_praksa' to get their IDs
      $query = $this->entityTypeManager()->getStorage('node')->getQuery()
        ->condition('type', 'products_for_praksa')
        ->latestRevision()
        ->condition('field_products_tags.entity:taxonomy_term.name', $currentlySelected, '=')
        ->execute();

      //Getting all nodes into one
      $nodes = $node_storage->loadMultiple($query);

      return $nodes;
    }
    if($twigValue != null and $currentlySelected != '') {
      //Getting typeManager for type node that we will later use to get all nodes into one node
      $node_storage = $this->EntityTypeManager()->getStorage('node');

      //Going trough entities of type 'products_for_praksa' to get their IDs
      $query = $this->entityTypeManager()->getStorage('node')->getQuery()
        ->condition('type', 'products_for_praksa')
        ->condition('title', $twigValue)
        ->latestRevision()
        ->condition('field_products_tags.entity:taxonomy_term.name', $currentlySelected, '=')
        ->execute();

      //Getting all nodes into one
      $nodes = $node_storage->loadMultiple($query);

      return $nodes;
    }

  }

  /**
   * @return array|mixed|null
   * Getting number entered in configuration on '/control-number'
   */
  private function getValueFromForm(){
    $config = \Drupal::configFactory()->get('return_products.custom_control');
    $value = $config->get('enteredNumber');
    return $value;
  }

  /**
   * @param $pageNumber
   * @param $products
   * @param $productsOnPage
   * @return array
   * Get products to be displayed on current page
   */
  private function getProductsForPage($pageNumber, $products, $productsOnPage) {
    $start = ($pageNumber-1) * $productsOnPage;
    $offset = 1;
    $toProduct = 1;
    $productsToDisplay = array();
    foreach ($products as $product) {
      if($offset > $start) {
        if($toProduct <= $productsOnPage) {
          $productsToDisplay[] = $product;
          $toProduct++;
        }
      }
      $offset++;
    }

//    $end = $start + $productsOnPage;
//    $productsToDisplay = array_slice($products, $start, $end, TRUE);

    return $productsToDisplay;
  }

  /**
   * @param $products
   * @param $productsOnPage
   * @return float
   * Return number of pages
   */
  private function getNumberOfPages($products, $productsOnPage) {
    $numberOfPages = count($products) / $productsOnPage;
    return ceil($numberOfPages);
  }

  /**
   * @param $product One node
   * @return array All tags inside this node
   * Getting all tags from one node
   */
  private function getTaxonomy($product) {
    $allTags = $product->get('field_products_tags')->referencedEntities();
    $tags= [];
    foreach ($allTags as $one) {
      $tag = $one -> getName();
      $tags[] = $tag;
    }
    return $tags;
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * Getting all taxonomy terms in vocabulary Product tags (machine name: products)
   */
  private function getTaxonomyTerms() {
    $vid = 'products';
    $term_data = [];
    $terms = $this->EntityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $term_data[] = array(
        'name' => $term->name
      );
    }
    return $term_data;
  }

  /**
   * @param $nodes
   *
   * @return array
   * Getting just products that don't have parents
   */
  public function getProductsNoParents($nodes) {
    $products = [];
    foreach ($nodes as $product) {
      $file = $product->field_image->entity;
      $test = $product->field_parent->getValue();
      if(count($test) == 0) {
        $products[] = array(
          'title' => $product->title->value,
          'image' => file_url_transform_relative(file_create_url($file->getFileUri())),
          'description' => $product->body->value,
          'tags' => $this->getTaxonomy($product),
        );
      }
    }
    return $products;
  }

  private function getMenuItemId() {
    $my_menu = $this->entityTypeManager->getStorage('menu_link_content')
      ->loadByProperties(['menu_name' => 'main']);
    $currentId = '20';
    foreach ($my_menu as $menu_item) {
      $title = $menu_item->getTitle();
      if($title == 'No parents') {
        $currentId = $menu_item->id();
        break;
      }
    }
    return $currentId;
  }

  /**
   * Getting all children products
   */
  public function getProductsChildren($nodes) {
    $products = [];
    foreach ($nodes as $product) {
      $file = $product->field_image->entity;
      $test = $product->field_parent->getValue();
      if(count($test) != 0) {
        $products[] = array(
          'title' => $product->title->value,
          'image' => file_url_transform_relative(file_create_url($file->getFileUri())),
          'description' => $product->body->value,
          'tags' => $this->getTaxonomy($product),
        );
      }
    }
    return $products;
  }


}
